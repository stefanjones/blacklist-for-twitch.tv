# Blacklist for Twitch.tv (GreaseMonkey Script)#

### Requirements ###
* Install the [GreaseMonkey Extension for Firefox](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)
* Download the .user.js file from this repository and install it using GreaseMonkey
* List any games you would like to block in the **blacklist** section of the user script

### What does this script do? ###
* Hides any games that you would prefer to not see when browsing Twitch.tv
* Creates a header link on the Games and Channels pages to quickly unhide your search results
* Filters any games that are added on-the-fly when scrolling down the page
* Removes "Free With Twitch Prime" ad from the Sidebar
* Removes "Promoted Game" section from the Sidebar
* Removes "Promoted Game" content in the Directory

### Known bugs in the current version ###
* If too many streams are filtered at once, you may need to resize the screen to load the second page of content.

### How do I use it? ###
* Add items to the 'blacklist' variable in the user script to hide them as the page loads. The name must match the spelling used on Twitch.tv (i.e. "ArmA III")
```
#!javascript
    // The default blacklist, blocks MOBA-style games
    var blacklist = 
    ['League of Legends', 'Dota 2', 'Heroes of the Storm'];
```
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* stefan.jones@usask.ca
