// ==UserScript==
// @name        Blacklist for Twitch
// @namespace   hiddentwitch
// @description Create a list of games to hide when visiting twitch.tv.
// @include     http://twitch.tv/directory*
// @include     https://twitch.tv/directory*
// @include     https://www.twitch.tv/directory*
// @include     http://www.twitch.tv/directory*
// @exclude     *?showall
// @version     0.8
// @grant       none
// ==/UserScript==
// @feature    Newly added elements that match are blocked when scrolling down the page.
// @feature    Results are re-filtered after changing pages using the Twitch sidebar
// @feature    Hide the large Twitch Prime ads in the sidebar
// @feature    Hide promoted game ads in the sidebar and directory
// @bug        If too many items are hidden on the first page, resize to load more results.
var blacklist = [
  'League of Legends',
  'Dota 2',
  'Heroes of the Storm'
];

// When enabled, lists games that matched the blacklist in the javascript console
var debug = false;

Array.prototype.contains = function (obj) { var i = this.length; while (i--) { if (this[i] === obj) { return true; } } return false; };

/* Title Listener: Watch for changes to the title from using in-page navigation */
function injectTitleListener() {

  var titleTarget = document.querySelector('head > title');
  var observer = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
      for (var i = 0; i < mutation.addedNodes.length; i++) {
       if (debug) {
          var newTitle = mutation.addedNodes[i];
          console.log(newTitle);
        }
        
        //Re-inject our listeners and navigation and refilter if necessary
        injectItemsListener();
        filterDirectory();
        if (document.getElementById('hiddenCount') === null) {
          addBreadcrumbNav();
        }
      }
    });
  });
  
  var configTarget = {
    // A Text node is added as a child node when the title changes.
    childList: true
  }
  observer.observe(titleTarget, configTarget);
  
  // Clean up references to Mutation Observer
  window.addEventListener('unload', function () {
    console.log('Disconnecting Title listener');
    observer.disconnect();
  }, false);
}

/* Items Listener: Hides matching games on-the-fly as they are added to the page */
function injectItemsListener() {
  
  // Hide streams that appear in target element matching criteria
  var target = document.querySelector('.items>div');
  
  var observer = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
      for (var i = 0; i < mutation.addedNodes.length; i++) {
        var game = mutation.addedNodes[i];
        var matchedTitle = null;
        var gameInner = game.querySelector('a.boxart');
        if (gameInner !== undefined && gameInner !== null) {
          // Top Streams
          matchedTitle = gameInner.getAttribute('title');
          // Random Streams
          if (matchedTitle === null) {
            matchedTitle = gameInner.getAttribute('original-title');
          }
          if (blacklist.contains(matchedTitle)) {
            game.style.display = 'none';
            if (debug) {
              console.log(matchedTitle + ' filtered via listener');
            }
          }
        }
      }
    });
  });
  
  var config = {
    childList: true
  }
  observer.observe(target, config);
  
  // Clean up references to Mutation Observer
  window.addEventListener('unload', function () {
    observer.disconnect();
  }, false);
}

/*addBreadcrumbNav: Insert the 'Show Hidden Channels/Games' link */
function addBreadcrumbNav() {
  var aNotice = document.createElement('a');
  var liNotice = document.createElement('li');
  aNotice.setAttribute('href', '?showall');
  aNotice.setAttribute('id', 'hiddenCount');
  liNotice.appendChild(aNotice);
  
  var channelNav = document.getElementsByClassName('nav');
  var gamesHeader = document.getElementById('custom_filter');
  
  // Insert onto Channels Directory
  if (typeof channelNav[0] !== 'undefined') {
    var navBar = channelNav[0];
    var aContent = document.createTextNode('Show Hidden Channels');
    aNotice.appendChild(aContent);
    navBar.insertBefore(liNotice, navBar.firstChild);
  }
  
  // Insert onto Games Directory
   else if (typeof gamesHeader !== 'undefined') {
    var aContent = document.createTextNode('Show Hidden Games');
    aNotice.setAttribute('style', 'display:block; text-align:right');
    aNotice.setAttribute('title', 'Reload this page to see the full list of games');
    aNotice.appendChild(aContent);
    gamesHeader.insertBefore(aNotice, gamesHeader.firstChild);
  } 
  
  else {
    if (debug) {
      console.log('GreaseMonkey cannot detect the in-page navigation.');
    }
  }
}
/* filterDirectory: Runs a filter on all listed elements after the page fully loads */
function filterDirectory() {
  var numHidden = 0;
  var games = document.querySelectorAll('a.game-item');
  var box = document.querySelectorAll('a.boxart');
  var offers = document.querySelectorAll('dl.js-offers');
  var promotedSidebar = document.querySelectorAll('dd.promotedGames');
  var promotedHeader = document.querySelectorAll('div.js-directory-featured-game'); 
  
  /* On Games Pages */
  for (var i = 0; i < games.length; i++)
  {
    var gameTitle = games[i].getAttribute('title');
    if (gameTitle !== null) {
      if (blacklist.contains(gameTitle)) {
        games[i].parentNode.parentNode.style.display = 'none';
        numHidden++;
        if (debug) {
          console.log(gameTitle + ' filtered via games');
        }
      }
    }
  }
  
  /* On Streams Pages */
  for (var i = 0; i < box.length; i++)
  {
    var popularTitle = box[i].getAttribute('title');
    if (popularTitle != null) {
      if (blacklist.contains(popularTitle)) {
        box[i].parentNode.parentNode.parentNode.parentNode.style.display = 'none';
        numHidden++;
        if (debug) {
          console.log(popularTitle + ' filtered via channels');
        }
      }
    }
  }
  
  /* On any page, remove Twitch Prime offers */
  for (var i = 0; i < offers.length; i++)
  {
    offers[i].style.display = 'none';
    if (debug) {
          console.log('Removed js-offers');
    }  
  }
  
  /* On any page, remove Promoted Game Sidebar */
  for (var i = 0; i < promotedSidebar.length; i++)
  {
    promotedSidebar[i].style.display = 'none';
    if (debug) {
          console.log('Removed promoted sidebar');
    }  
  }
  
  /* On any page, remove Promoted Game Header */
  for (var i = 0; i < promotedHeader.length; i++)
  {
    promotedHeader[i].style.display = 'none';
    if (debug) {
          console.log('Removed promoted content header');
    }  
  }
  
  if (numHidden > 0) {
    if (document.getElementById('hiddenCount') == null) {
      addBreadcrumbNav();
    }
    if (debug) {
      console.log('Filtered ' + numHidden + ' results on load');
    }
  }
}

/* Prepare to perform actions after Page Load */
window.addEventListener('load', function () {
  filterDirectory();
  injectItemsListener();
  injectTitleListener();
}, false);
